import type { Schema, Attribute } from '@strapi/strapi';

export interface CheckInCheckIn extends Schema.Component {
  collectionName: 'components_check_in_check_ins';
  info: {
    displayName: 'checkIn';
    description: '';
  };
  attributes: {
    user: Attribute.Relation<
      'check-in.check-in',
      'oneToOne',
      'plugin::users-permissions.user'
    >;
    dateTime: Attribute.DateTime;
  };
}

export interface CheckOutCheckOut extends Schema.Component {
  collectionName: 'components_check_out_check_outs';
  info: {
    displayName: 'checkOut';
  };
  attributes: {
    user: Attribute.Relation<
      'check-out.check-out',
      'oneToOne',
      'plugin::users-permissions.user'
    >;
    dateTime: Attribute.DateTime;
  };
}

declare module '@strapi/types' {
  export module Shared {
    export interface Components {
      'check-in.check-in': CheckInCheckIn;
      'check-out.check-out': CheckOutCheckOut;
    }
  }
}
