'use strict';

/**
 * rfid-card router
 */

const { createCoreRouter } = require('@strapi/strapi').factories;

module.exports = createCoreRouter('api::rfid-card.rfid-card');
