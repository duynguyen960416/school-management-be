module.exports = {
  routes: [
    { // Path defined with a regular expression
      method: 'GET',
      path: '/rfid-cards/:cardId', // Only match when the URL parameter is composed of lowercase letters
      handler: 'findcard.findCardById',
    }
  ]
}