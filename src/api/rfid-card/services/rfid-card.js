'use strict';

/**
 * rfid-card service
 */

const { createCoreService } = require('@strapi/strapi').factories;

module.exports = createCoreService('api::rfid-card.rfid-card');
