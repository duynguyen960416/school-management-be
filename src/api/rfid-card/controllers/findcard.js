"use-strict";

module.exports = {
  async findCardById(ctx, next) {
    try {
      const cardId = ctx.request.url.split("/")[3];
      const data = await strapi.db.query("api::rfid-card.rfid-card").findOne(
     {where:{ cardId:cardId}}
      );
      ctx.send(data);
    } catch (error) {
      console.log(error);
    } 
  },
};