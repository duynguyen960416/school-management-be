'use strict';

/**
 * rfid-card controller
 */
// @ts-ignore
const { createCoreController } = require('@strapi/strapi').factories;

module.exports = createCoreController('api::rfid-card.rfid-card');

