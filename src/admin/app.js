// @ts-nocheck
// @ts-ignore
import  auth_image  from "./extensions/auth_image.png";
import  favicon  from "./extensions/favicon.ico";
const config = {
  locales: [
    // 'ar',
    // 'fr',
    // 'cs',
    // 'de',
    // 'dk',
    // 'es',
    // 'he',
    // 'id',
    // 'it',
    // 'ja',
    // 'ko',
    // 'ms',
    // 'nl',
    // 'no',
    // 'pl',
    // 'pt-BR',
    // 'pt',
    // 'ru',
    // 'sk',
    // 'sv',
    // 'th',
    // 'tr',
    // 'uk',
    // 'vi',
    // 'zh-Hans',
    // 'zh',
  ],
  translations: {
    en: {
      "app.components.LeftMenu.navbrand.title": "Thuy Loi Unviversity", 
      "app.components.LeftMenu.navbrand.workplace": "Admin dashboard",
      "Auth.form.welcome.title": "Welcome to TLS",    
      "Auth.form.welcome.subtitle": "Log in to your Admin account",    
      "HomePage.helmet.title": "TLS Dashboard",
      "app.components.HomePage.welcomeBlock.content.again": "Chào mừng quay trở lại",
    }
  },
  auth:{
    logo: auth_image,
  },
  head:{
    favicon: favicon
  },
  menu: {
    logo: auth_image
  },
  tutorials: false,
  
};

const bootstrap = (app) => {
  console.log(app);
};

export default {
  config,
  bootstrap,
};
